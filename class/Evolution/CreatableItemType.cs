
namespace Evolution {

  public class CreatableItemType {
    string id;
    string description;
    string menuDescription;
    string tooltip;
    string menuShortcut;
    string iconName;

    public CreatableItemType (string id, string description, string menuDescription, string tooltip, string menuShortcut, string iconName) {
      this.id = id;
      this.description = description;
      this.menuDescription = menuDescription;
      this.tooltip = tooltip;
      this.menuShortcut = menuShortcut;
      this.iconName = iconName;
    }

  }

}
