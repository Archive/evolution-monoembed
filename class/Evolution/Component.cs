using System;
using System.Runtime.CompilerServices;
using System.Reflection;
using System.Text;

using Gtk;

namespace Evolution {

  public abstract class Component {
    public Component () {
      Application.Init();
    }

    public abstract void CreateControls (out IntPtr sidebar_widget_handle, out IntPtr view_widget_handle, out IntPtr statusbar_widget_handle);
    
    public abstract CreatableItemType[] GetUserCreatableItems ();
    
    public abstract void RequestCreateItem (string item_type_name);

    [MethodImplAttribute(MethodImplOptions.InternalCall)]
    extern void SetUIXML (string ui_xml);

    [MethodImplAttribute(MethodImplOptions.InternalCall)]
    extern public void SetVerbs (string[] verbs);

    public void SetUI (Assembly assembly, string resource_name) {
      if (assembly == null)
	assembly = Assembly.GetCallingAssembly ();

      System.IO.Stream s = assembly.GetManifestResourceStream (resource_name);

      if (s == null)
	throw new ArgumentException ("Cannot get resource file '" + resource_name + "'",
				     "resource_name");

      int size = (int) s.Length;
      byte[] buffer = new byte[size];
      s.Read (buffer, 0, size);
      s.Close ();

      ui_xml = (new UTF8Encoding()).GetString (buffer);
    }

    public void ActivateUI () {
      SetUIXML (ui_xml);
    }

    public void DeactivateUI () {
    }

    public IntPtr Handle {
      get {
	return handle;
      }
      set {
	handle = value;
      }
    }

    string ui_xml;
    IntPtr handle;
  }

}
