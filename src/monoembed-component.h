/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/* monoembed-component.h
 *
 * Copyright (C) 2003  Ximian, Inc.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of version 2 of the GNU General Public
 * License as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 *
 * Author: Chris Toshok <toshok@ximian.com>
 */

#ifndef _MONOEMBED_COMPONENT_H_
#define _MONOEMBED_COMPONENT_H_

#include <bonobo/bonobo-object.h>
#include <mono/jit/jit.h>

#include "Evolution.h"

#define MONOEMBED_TYPE_COMPONENT			(monoembed_component_get_type ())
#define MONOEMBED_COMPONENT(obj)			(G_TYPE_CHECK_INSTANCE_CAST ((obj), MONOEMBED_TYPE_COMPONENT, MonoEmbedComponent))
#define MONOEMBED_COMPONENT_CLASS(klass)		(G_TYPE_CHECK_CLASS_CAST ((klass), MONOEMBED_TYPE_COMPONENT, MonoEmbedComponentClass))
#define MONOEMBED_IS_COMPONENT(obj)			(G_TYPE_CHECK_INSTANCE_TYPE ((obj), MONOEMBED_TYPE_COMPONENT))
#define MONOEMBED_IS_COMPONENT_CLASS(klass)		(G_TYPE_CHECK_CLASS_TYPE ((obj), MONOEMBED_TYPE_COMPONENT))


typedef struct _MonoEmbedComponent        MonoEmbedComponent;
typedef struct _MonoEmbedComponentPrivate MonoEmbedComponentPrivate;
typedef struct _MonoEmbedComponentClass   MonoEmbedComponentClass;

struct _MonoEmbedComponent {
	BonoboObject parent;

	MonoEmbedComponentPrivate *priv;
};

struct _MonoEmbedComponentClass {
	BonoboObjectClass parent_class;

	POA_GNOME_Evolution_Component__epv epv;
};


GType monoembed_component_get_type (void);

MonoEmbedComponent *monoembed_component_new (const char *component_alias, MonoDomain *domain, MonoAssembly *assembly, MonoClass *class);

void monoembed_component_add_internal_calls (void) ;

#endif /* _MONOEMBED_COMPONENT_H_ */
