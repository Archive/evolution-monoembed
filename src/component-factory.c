/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/* component-factory.c - Factory for Evolution's MonoEmbed component.
 *
 * Copyright (C) 2002 Ximian, Inc.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of version 2 of the GNU General Public
 * License as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 *
 * Author: Chris Toshok <toshok@ximian.com>
 */

#include <config.h>

#include <string.h>

#include <bonobo/bonobo-shlib-factory.h>
#include <bonobo/bonobo-exception.h>

#include <e-util/e-icon-factory.h>

#include <mono/jit/jit.h>
#include <mono/metadata/assembly.h>

#include "monoembed-component.h"


#define FACTORY_ID "OAFIID:GNOME_EvolutionMonoEmbed_Factory_2"
#define EMBED_PREFIX "OAFIID:GNOME_EvolutionMonoEmbed_"

static GHashTable *components = NULL;

static void
component_dead (gpointer data, GObject *object)
{
	char *component_id = data;

	g_hash_table_remove (components, component_id);
	g_free (component_id);
}

static BonoboObject *
factory (BonoboGenericFactory *factory,
	 const char *component_id,
	 void *closure)
{
	Bonobo_ServerInfoList *info_list;
	static MonoDomain *initial_domain = NULL;
	CORBA_Environment ev;
	char *query;
	const char *assembly_path;
	const char *component_alias;
	char *class_name;
	char *namespace;
	MonoDomain   *domain;
	MonoAssembly *assembly;	
	MonoClass    *class;
	char *sep;
	BonoboObject *rv;
	char *key;

	printf ("asked to activate component_id `%s'\n", component_id);

	if (!components)
		components = g_hash_table_new (g_str_hash, g_str_equal);

	rv = g_hash_table_lookup (components, component_id);
	if (rv)
		return rv;

	CORBA_exception_init (&ev);

	query = g_strdup_printf ("iid == '%s'", component_id);

	info_list = bonobo_activation_query (query, NULL, &ev);

	g_free (query);

	if (ev._major != CORBA_NO_EXCEPTION) {
		printf ("Bonobo activation failure: %s\n",
			bonobo_exception_get_text (&ev));
		CORBA_exception_free (&ev);
		return NULL;
	}

	printf ("info_list->_length == %d\n", info_list->_length);

	if (info_list->_length == 0) {
		g_warning (FACTORY_ID ": Don't know what to do with `%s', as it nothing was returned from the bonobo-activation query.", component_id);
		return NULL;
	}
		
	assembly_path   = bonobo_server_info_prop_lookup (&info_list->_buffer[0], "evolution_monoembed:assembly", NULL);
	class_name      = g_strdup (bonobo_server_info_prop_lookup (&info_list->_buffer[0], "evolution_monoembed:class", NULL));
	component_alias = bonobo_server_info_prop_lookup (&info_list->_buffer[0], "evolution:component_alias", NULL);

	if (!assembly_path || !class_name) {
		g_warning (FACTORY_ID ": Don't know what to do with `%s', as it's missing assembly and/or class attributes.", component_id);
		return NULL;
	}

	sep = strrchr(class_name, '.');
	if (sep) {
		*sep = '\0';
		namespace = class_name;
		class_name = sep + 1;
	}
	else {
		namespace = "";
	}

	g_warning ("embedded mono assembly = %s\n", assembly_path);

	if (!initial_domain) {
		mono_config_parse ("/etc/mono/config");
		initial_domain = domain = mono_jit_init ("Evolution Mono Embedding Factory Initial Domain");

		monoembed_component_add_internal_calls();
	}
	else {
		domain = mono_domain_create ();
	}
	assembly = mono_domain_assembly_open (domain, assembly_path);
	if (!assembly) {
		g_warning (FACTORY_ID ": Unable to find assembly `%s'", assembly_path);
		mono_jit_cleanup (domain);
		return NULL;
	}

	g_warning ("loading class %s.%s\n", namespace, class_name);
	class = mono_class_from_name (mono_assembly_get_image (assembly), namespace, class_name);

	/* we found the assembly, create a EMonoEmbed object
	   to wrap it */
	key = g_strdup (component_id);
	rv = BONOBO_OBJECT (monoembed_component_new (component_alias, domain, assembly, class));
	g_hash_table_insert (components, key, rv);

	g_object_weak_ref (G_OBJECT (rv), component_dead, key);

	return rv;
}

BONOBO_ACTIVATION_SHLIB_FACTORY (FACTORY_ID, "Evolution Mono Component Factory", factory, NULL)
