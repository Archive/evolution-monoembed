/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/* monoembed-component.c
 *
 * Copyright (C) 2003  Ettore Perazzoli
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of version 2 of the GNU General Public
 * License as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 *
 * Author: Chris Toshok <toshok@ximian.com>
 */


#include <config.h>

#include <gtk/gtklabel.h>
#include <libgnome/gnome-i18n.h>
#include <bonobo/bonobo-control.h>
#include <bonobo/bonobo-ui-util.h>
#include <mono/metadata/debug-helpers.h>
#include "monoembed-component.h"
#include "shell/e-user-creatable-items-handler.h"

#define PARENT_TYPE bonobo_object_get_type ()
static BonoboObjectClass *parent_class = NULL;

struct _MonoEmbedComponentPrivate {
	char         *component_alias;
	MonoDomain   *domain;
	MonoAssembly *assembly;
	MonoClass    *class;
	MonoObject   *embedded;
	char         *cached_ui;
	BonoboControl *sidebar_control;
	BonoboControl *view_control;
	BonoboControl *statusbar_control;
	BonoboUIVerb  *verbs;
	EUserCreatableItemsHandler *creatable_items_handler;
};

typedef struct {
	MonoObject object;
	MonoString *id;
	MonoString *description;
	MonoString *menuDescription;
	MonoString *tooltip;
	MonoString *menuShortcut;
	MonoString *iconName;
} MonoEmbedCreatableItemType;

static MonoEmbedComponent*
get_handle (MonoObject *obj)
{
	MonoProperty *prop;
	MonoObject *rv;
	MonoEmbedComponent **pp;

	prop = mono_class_get_property_from_name (mono_object_get_class (obj), "Handle");

	rv = mono_property_get_value (prop, obj, NULL, NULL);

	pp = mono_object_unbox (rv);
	return *pp;
}

static void
set_handle (MonoEmbedComponent *embed)
{
	void *params[1];
	MonoProperty *prop;

	prop = mono_class_get_property_from_name (embed->priv->class, "Handle");

	params[0] = &embed;
	mono_property_set_value (prop, embed->priv->embedded, params, NULL);
}

static void
mono_set_ui (MonoObject *obj, MonoString *mono_str)
{
	MonoEmbedComponent *embed = get_handle (obj);
	char *string = g_utf16_to_utf8 (mono_string_chars (mono_str), mono_string_length (mono_str),
					NULL, NULL, NULL);
	char *ui;
	BonoboUIComponent *uic = bonobo_control_get_ui_component (embed->priv->view_control);

	if (embed->priv->verbs) {
		bonobo_ui_component_add_verb_list_with_data (uic, embed->priv->verbs, embed);
	}
	else {
		g_warning ("no verbs specified");
	}

	if (embed->priv->cached_ui) {
		ui = embed->priv->cached_ui;
	}
	else {
		BonoboUINode *node;

		node = bonobo_ui_node_from_string (string);

		bonobo_ui_util_translate_ui (node);

		bonobo_ui_util_fixup_icons (node);

		ui = bonobo_ui_node_to_string (node, TRUE);

		embed->priv->cached_ui = ui;
	}

	if (ui && uic) {
		bonobo_ui_component_freeze (uic, NULL);

		bonobo_ui_component_set (uic, "/", ui, NULL);

		e_user_creatable_items_handler_activate (embed->priv->creatable_items_handler, uic);

		bonobo_ui_component_thaw (uic, NULL);
	}

	g_free (string);
}

static void
monoembed_verb_cb (BonoboUIComponent *component,
		   MonoEmbedComponent *embed,
		   const char *verb)
{
	MonoMethod *method;
        MonoMethodDesc* mdesc;
	char *sig;

	sig = g_strconcat (":", verb, NULL);

	mdesc = mono_method_desc_new (sig, FALSE);
	method = mono_method_desc_search_in_class (mdesc, embed->priv->class);

	if (method)
		mono_runtime_invoke (method, embed->priv->embedded, NULL, NULL);
	else {
		g_warning ("No method for verb `%s'", verb);
	}

	g_free (sig);
}

static void
mono_set_verbs (MonoObject *obj, MonoArray *verb_array)
{
	MonoEmbedComponent *embed = get_handle (obj);
	int len = mono_array_length (verb_array);
	int i;

	g_warning ("in mono_set_verbs");

	if (embed->priv->verbs)
		g_free (embed->priv->verbs);

	embed->priv->verbs = g_new0 (BonoboUIVerb, len + 1);

	for (i = 0; i < mono_array_length (verb_array); i ++) {
		MonoString *str = mono_array_get (verb_array, MonoString*, i);
		char *string = g_utf16_to_utf8 (mono_string_chars (str), mono_string_length (str),
						NULL, NULL, NULL);
		embed->priv->verbs[i].cname = string; /* XXX leaked */
		embed->priv->verbs[i].cb = (BonoboUIVerbFn)monoembed_verb_cb;
	}
}

void
monoembed_component_add_internal_calls (void) 
{
	mono_add_internal_call ("Evolution.Component::SetUIXML(string)", mono_set_ui);
	mono_add_internal_call ("Evolution.Component::SetVerbs(string[])", mono_set_verbs);
}

/* Evolution::Component CORBA methods.  */

static void
control_activate_cb (BonoboControl *control, 
		     gboolean activate, 
		     MonoEmbedComponent *embed)
{
        MonoMethodDesc* mdesc;
	MonoMethod *method;
	char *sig;

	if (activate) {
		Bonobo_UIContainer remote_ui_container;
		BonoboUIComponent *uic;

		uic = bonobo_control_get_ui_component (control);

		remote_ui_container = bonobo_control_get_remote_ui_container (control, NULL);
		bonobo_ui_component_set_container (uic, remote_ui_container, NULL);
		bonobo_object_release_unref (remote_ui_container, NULL);

		sig = ":Activate" ;
	}
	else {
		BonoboUIComponent *uic;

		uic = bonobo_control_get_ui_component (control);

		bonobo_ui_component_unset_container (uic, NULL);

		sig = ":Deactivate" ;
	}

	mdesc = mono_method_desc_new (sig, FALSE);
	method = mono_method_desc_search_in_class (mdesc, embed->priv->class);

	if (method)
		mono_runtime_invoke (method, embed->priv->embedded, NULL, NULL);
}

#define METHOD_SIG ":CreateControls"
static void
impl_createControls (PortableServer_Servant servant,
		     Bonobo_Control *corba_sidebar_control,
		     Bonobo_Control *corba_view_control,
		     Bonobo_Control *corba_statusbar_control,
		     CORBA_Environment *ev)
{
	MonoEmbedComponent *monoembed = MONOEMBED_COMPONENT (bonobo_object_from_servant (servant));
	MonoMethod *method;
        MonoMethodDesc* mdesc;
	GtkWidget *sidebar_widget;
	GtkWidget *view_widget;
	GtkWidget *statusbar_widget;
	BonoboControl *sidebar_control;
	BonoboControl *view_control;
	BonoboControl *statusbar_control;

	mdesc = mono_method_desc_new (METHOD_SIG, FALSE);
	method = mono_method_desc_search_in_class (mdesc, monoembed->priv->class);

	if (method) {
		gpointer params[3];

		params[0] = &sidebar_widget;
		params[1] = &view_widget;
		params[2] = &statusbar_widget;

		mono_runtime_invoke (method, monoembed->priv->embedded, params, NULL);
	}
	else {
		sidebar_widget = gtk_label_new (_("Unable to create control in mono"));
		view_widget = gtk_label_new (_("Unable to create control in mono"));
		statusbar_widget = gtk_label_new (_("Unable to create control in mono"));
	}

	gtk_widget_show (sidebar_widget);
	sidebar_control = bonobo_control_new (sidebar_widget);
	*corba_sidebar_control = CORBA_Object_duplicate (BONOBO_OBJREF (sidebar_control), ev);

	gtk_widget_show (view_widget);
	view_control = bonobo_control_new (view_widget);
	*corba_view_control = CORBA_Object_duplicate (BONOBO_OBJREF (view_control), ev);

	gtk_widget_show (statusbar_widget);
	statusbar_control = bonobo_control_new (statusbar_widget);
	*corba_statusbar_control = CORBA_Object_duplicate (BONOBO_OBJREF (statusbar_control), ev);

	monoembed->priv->view_control = view_control;
	monoembed->priv->sidebar_control = sidebar_control;
	monoembed->priv->statusbar_control = statusbar_control;

	monoembed->priv->creatable_items_handler = e_user_creatable_items_handler_new (monoembed->priv->component_alias, NULL, NULL);
	g_signal_connect (view_control, "activate", G_CALLBACK (control_activate_cb), monoembed);
}

static GNOME_Evolution_CreatableItemTypeList *
impl__get_userCreatableItems (PortableServer_Servant servant,
                              CORBA_Environment *ev)
{
        GNOME_Evolution_CreatableItemTypeList *list = GNOME_Evolution_CreatableItemTypeList__alloc ();
	MonoEmbedComponent *monoembed = MONOEMBED_COMPONENT (bonobo_object_from_servant (servant));
	MonoMethod *method;
        MonoMethodDesc *mdesc;
        MonoArray *array;
        MonoEmbedCreatableItemType *item;
        gchar *id, *description, *menuDescription, *tooltip, *menuShortcut, *iconName;
        int i;

	mdesc = mono_method_desc_new (":GetUserCreatableItems", FALSE);
	method = mono_method_desc_search_in_class (mdesc, monoembed->priv->class);

	if (method) {
		g_warning ("calling GetUserCreatableItems method");

		array = (MonoArray *) mono_runtime_invoke (method, monoembed->priv->embedded, NULL, NULL);
		list->_length = mono_array_length (array);
		list->_maximum = list->_length;
        	list->_buffer = GNOME_Evolution_CreatableItemTypeList_allocbuf (list->_length);
        	
	        CORBA_sequence_set_release (list, TRUE);

		for (i = 0; i < list->_length; i++) {
			item = mono_array_get (array, MonoEmbedCreatableItemType *, i);

		        id = mono_string_to_utf8 (item->id);
		        description = mono_string_to_utf8 (item->description);
		        menuDescription = mono_string_to_utf8 (item->menuDescription);
		        tooltip = mono_string_to_utf8 (item->tooltip);
		        menuShortcut = mono_string_to_utf8 (item->menuShortcut);
		        iconName = mono_string_to_utf8 (item->iconName);

		        list->_buffer[i].id = CORBA_string_dup (id);
		        list->_buffer[i].description = CORBA_string_dup (description);
		        list->_buffer[i].menuDescription = CORBA_string_dup (menuDescription);
		        list->_buffer[i].tooltip = CORBA_string_dup (tooltip);
		        list->_buffer[i].menuShortcut = menuShortcut[0]; // FIXME: what is the proper way of doing this?
		        list->_buffer[i].iconName = CORBA_string_dup (iconName);
		        list->_buffer[i].type = GNOME_Evolution_CREATABLE_OBJECT; // FIXME: this should be controllable by managed code
		        
		        g_free (id);
		        g_free (description);
		        g_free (menuDescription);
		        g_free (tooltip);
		        g_free (menuShortcut);
		        g_free (iconName);
		}
	}
	else {
		g_warning ("no GetUserCreatableItems method");
	        list->_length = 0;
	        list->_maximum = list->_length;
	        list->_buffer = GNOME_Evolution_CreatableItemTypeList_allocbuf (list->_length);
                                                                                
	        CORBA_sequence_set_release (list, FALSE);
	}
                                                                                
        return list;
}

static void
impl_requestCreateItem (PortableServer_Servant servant,
                        const CORBA_char *item_type_name,
                        CORBA_Environment *ev)
{
	MonoEmbedComponent *monoembed = MONOEMBED_COMPONENT (bonobo_object_from_servant (servant));
	MonoMethod *method;
        MonoMethodDesc *mdesc;

	mdesc = mono_method_desc_new (":RequestCreateItem", FALSE);
	method = mono_method_desc_search_in_class (mdesc, monoembed->priv->class);

	if (method) {
		gpointer params[1];
		params[0] = (gpointer) mono_string_new (monoembed->priv->domain, item_type_name); // FIXME: what encoding does CORBA use?
		mono_runtime_invoke (method, monoembed->priv->embedded, params, NULL);
	}
}


/* GObject methods.  */

static void
impl_dispose (GObject *object)
{
	MonoEmbedComponent *component = MONOEMBED_COMPONENT (object);

	if (component->priv) {
		g_free (component->priv->component_alias);

		g_free (component->priv);

		if (component->priv->creatable_items_handler)
			g_object_unref (component->priv->creatable_items_handler);

		component->priv = NULL;
	}

	(* G_OBJECT_CLASS (parent_class)->dispose) (object);
}

static void
impl_finalize (GObject *object)
{
	MonoEmbedComponentPrivate *priv = MONOEMBED_COMPONENT (object)->priv;

	g_free (priv);

	(* G_OBJECT_CLASS (parent_class)->finalize) (object);
}


/* Initialization.  */

static void
monoembed_component_class_init (MonoEmbedComponentClass *class)
{
	POA_GNOME_Evolution_Component__epv *epv = &class->epv;
	GObjectClass *object_class = G_OBJECT_CLASS (class);

	epv->createControls = impl_createControls;
	epv->_get_userCreatableItems = impl__get_userCreatableItems;
	epv->requestCreateItem = impl_requestCreateItem;

	object_class->dispose  = impl_dispose;
	object_class->finalize = impl_finalize;

	parent_class = g_type_class_peek_parent (class);
}

static void
monoembed_component_init (MonoEmbedComponent *component)
{
	MonoEmbedComponentPrivate *priv;

	priv = g_new0 (MonoEmbedComponentPrivate, 1);

	component->priv = priv;
}


/* Public API.  */

MonoEmbedComponent *
monoembed_component_new (const char *component_alias, MonoDomain *domain, MonoAssembly *assembly, MonoClass *class)
{
	MonoEmbedComponent *component = g_object_new (monoembed_component_get_type (), NULL);

	component->priv->component_alias = g_strdup (component_alias);
	component->priv->domain = domain;
	component->priv->assembly = assembly;
	component->priv->class = class;
	component->priv->view_control = NULL;
	component->priv->sidebar_control = NULL;
	component->priv->statusbar_control = NULL;
	component->priv->verbs = NULL;

	component->priv->embedded = mono_object_new (component->priv->domain, component->priv->class);

	/* this has to come before the call the
	   mono_runtime_object_init so the Handle will be
	   available for mono_set_ui and mono_set_verbs if the
	   ctor calls either/both of those */
	set_handle (component);

	mono_runtime_object_init (component->priv->embedded);

	return component;
}

BONOBO_TYPE_FUNC_FULL (MonoEmbedComponent, GNOME_Evolution_Component, PARENT_TYPE, monoembed_component)
