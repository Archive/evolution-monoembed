
using System;

using Gtk;
using GtkSharp;
using Gnome;

using Evolution;

public class Example : Component {

  public Example () {
    SetUI (null, "example-ui.xml");
    SetVerbs (verbs);

    logo = new Gdk.Pixbuf (null, "mono_logo.png");
  }

  public override void CreateControls (out IntPtr sidebar_widget_handle, out IntPtr view_widget_handle, out IntPtr statusbar_widget_handle) {
    sidebar_widget = new Gtk.Label ("Mono Sidebar Control!");
    view_widget = new Gtk.Label ("Mono View Control!");
    statusbar_widget = new Gtk.Label ("Mono Statusbar Control!");

    sidebar_widget_handle = sidebar_widget.Handle;
    view_widget_handle = view_widget.Handle;
    statusbar_widget_handle = statusbar_widget.Handle;
  }

  public override CreatableItemType[] GetUserCreatableItems () {
    CreatableItemType[] types = new CreatableItemType[1];

    Console.WriteLine ("in GetUserCreatableItems");

    types[0] = new CreatableItemType ("test", "New Test", "_Test", "Create a new test item", "i", "stock_calendar");
    
    return types;
  }

  public override void RequestCreateItem (string item_type_name) {
    if (item_type_name == "test") {
      ((Gtk.Label) view_widget).Text = "Test item created!";
    }
  }

  public void Activate () {
    Console.WriteLine ("in Example.Activate()");
    ActivateUI();
  }

  public void Deactivate () {
    Console.WriteLine ("in Example.Deactivate()");
    DeactivateUI();
  }

  // Our verbs.  we need to define a method named the same thing as
  // the verb.
  string[] verbs = {
    "ExampleAbout"
  };

  public void ExampleAbout() {
    string[] authors = new string[1];
    authors[0] = "Chris Toshok <toshok@ximian.com>";
    About about = new About("evomonoembed-example.dll",
			    "0.1",
			    "Copyright (C) 2003 Ximian, Inc.",
			    "A simple example of an C# component embedded in Evolution.  It even has a BonoboUI merged menubar/toolbar.",
			    authors,
			    null, null, logo);

    about.Show();
  }

  Gdk.Pixbuf logo;
  Gtk.Widget sidebar_widget;
  Gtk.Widget view_widget;
  Gtk.Widget statusbar_widget;
}
