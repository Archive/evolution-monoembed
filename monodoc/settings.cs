using System;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace EvoMonodoc {

	public class SettingsHandler {
		static string settingsFile;
		static XmlSerializer settingsSerializer = new XmlSerializer (typeof (Settings));
		public static Settings Settings;
		
		static SettingsHandler ()
		{
			// >>> HACK HACK HACK
			// Kepp this only until we get Environment working right
			string rootDir = Environment.GetEnvironmentVariable ("XDG_CONFIG_HOME");
			if (rootDir == null || rootDir == "")
				rootDir = System.IO.Path.Combine (Environment.GetEnvironmentVariable ("HOME"), ".config");
			// <<< HACK HACK HACK
			
			Path = System.IO.Path.Combine (rootDir, "monodoc");
			settingsFile = System.IO.Path.Combine (Path, "settings.xml");
			if (File.Exists (settingsFile))
				Settings = (Settings) settingsSerializer.Deserialize (new XmlTextReader (settingsFile));
			else
				Settings = new Settings ();
		}

		public static void Save ()
		{
			EnsureSettingsDirectory ();
			settingsSerializer.Serialize (File.Create (settingsFile), Settings);
		}
		
		// these can be used for other types of settings too
		public static string Path;
		public static void EnsureSettingsDirectory ()
		{
			DirectoryInfo d = new DirectoryInfo (Path);
			if (!d.Exists)
				d.Create ();
		}
		
	}
		
	public class Settings {
		// public to allow serialization
		public bool EnableEditing = true;

		// Last serial number commited
		public int SerialNumber = 0;

		public string Email;
		public string Key;

		public static bool RunningGUI = false;
	}
}
