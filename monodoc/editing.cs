//
// editing.cs
//
// Author:
//   Ben Maurer (bmaurer@users.sourceforge.net)
//
// (C) 2003 Ben Maurer
//

using System;
using System.Collections;
using System.Collections.Specialized;
using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using System.Xml.XPath;
using System.Web;

namespace EvoMonodoc {
	public class EditingUtils {
		
		public static string GetEditUri (XPathNavigator n)
		{
			string ret = "edit:";
			
			ret += HttpUtility.UrlEncode (n.BaseURI);
			ret += "@";
			ret += HttpUtility.UrlEncode (GetXPath (n.Clone ()));
			
			return ret;
		}
		
		public static string GetXPath (XPathNavigator n)
		{
			switch (n.NodeType) {
				case XPathNodeType.Root: return "/";
				case XPathNodeType.Attribute: {
					string ret = "@" + n.Name;
					n.MoveToParent ();
					string s = GetXPath (n);
					return s + (s == "/" ? "" : "/") + ret;
				}
				break;
				case XPathNodeType.Element: {
					string ret = n.Name;
					int i = 1;
					while (n.MoveToPrevious ()) {
						if (n.NodeType == XPathNodeType.Element && n.Name == ret)
							i++;
					}
					ret += "[" + i + "]";
					if (n.MoveToParent ()) {
						string s = GetXPath (n);
						return s + (s == "/" ? "" : "/") + ret;
					}
				}
				break;
			}
			throw new Exception ("node type not supported for editing");
			
		}
		
		public static XmlNode GetNodeFromUrl (string url, RootTree tree)
		{
			string [] uSplit = ParseEditUrl (url);
			
			string xp = uSplit [2];
			int prov = int.Parse ( uSplit [0]);
			string id =  uSplit [1];
			
			XmlDocument d = tree.GetHelpSourceFromId (prov).GetHelpXmlWithChanges (id);
			
			return d.SelectSingleNode (xp);
		}
		
		public static void SaveChange (string url, RootTree tree, XmlNode node)
		{
			string [] uSplit = ParseEditUrl (url);
		
			string xp = uSplit [2];
			int prov = int.Parse (uSplit [0]);
			string id =  uSplit [1];
						
			HelpSource hs = tree.GetHelpSourceFromId (prov);
			
			changes.AddChange (hs.Name, hs.GetRealPath (id), xp, node);
			changes.Save ();
		}
		
		public static void RenderEditPreview (string url, RootTree tree, XmlNode new_node, XmlWriter w)
		{
			string [] uSplit = ParseEditUrl (url);
		
			string xp = uSplit [2];
			int prov = int.Parse (uSplit [0]);
			string id =  uSplit [1];
						
			HelpSource hs = tree.GetHelpSourceFromId (prov);
			hs.RenderPreviewDocs (new_node, w);
		}
		
		public static string [] ParseEditUrl (string url)
		{
			if (!url.StartsWith ("edit:"))
				throw new Exception ("wtf");
			
			string [] parts = url.Split ('@');
			if (parts.Length != 2)
				throw new Exception ("invalid editing url");
			
			string xp = HttpUtility.UrlDecode (parts [1]);
			parts = HttpUtility.UrlDecode (parts [0]).Substring ("edit:monodoc://".Length).Split ('@');
			
			int prov = int.Parse (parts [0]);
			
			return new string [] {parts [0], parts [1], xp};
		}
		
		public static void AccountForChanges (XmlDocument d, string docSet, string realFile)
		{
			FileChangeset fcs = changes.GetChangeset (docSet, realFile);
			if (fcs == null) return;
			
			foreach (Change c in fcs.Changes) {
				XmlNode old = d.SelectSingleNode (c.XPath);
				if (old != null)
					old.ParentNode.ReplaceChild (d.ImportNode (c.NewNode, true), old);
			}
		}
	
		static GlobalChangeset changes = GlobalChangeset.Load ();
	}

#region Data Model
	public class GlobalChangeset {
#region Load/Save
		static XmlSerializer serializer = new XmlSerializer (typeof (GlobalChangeset));
		static string changesetFile = Path.Combine (SettingsHandler.Path, "changeset.xml");
		
		public static GlobalChangeset Load ()
		{
			if (File.Exists (changesetFile))
				return (GlobalChangeset)serializer.Deserialize (new XmlTextReader (changesetFile));
			
			return new GlobalChangeset ();
		}
		
		public static GlobalChangeset LoadFromFile (string fileName)
		{
			return (GlobalChangeset)serializer.Deserialize (new XmlTextReader (fileName));
		}			
		
		public void Save ()
		{
			SettingsHandler.EnsureSettingsDirectory ();
			serializer.Serialize (File.Create (changesetFile), this);
		}
		
		static void VerifyDirectoryExists (DirectoryInfo d) {
			if (d.Exists)
				return;

			VerifyDirectoryExists (d.Parent);
			d.Create ();
		}
#endregion
		[XmlElement ("DocSetChangeset", typeof (DocSetChangeset))]
		public ArrayList DocSetChangesets = new ArrayList ();

		public FileChangeset GetChangeset (string docSet, string realFile)
		{
			foreach (DocSetChangeset dscs in DocSetChangesets) {
				if (dscs.DocSet != docSet) 
					continue;
			
				foreach (FileChangeset fcs in dscs.FileChangesets) {
					if (fcs.RealFile == realFile)
						return fcs;
				}
			}
			
			return null;
		}
		
		public void AddChange (string docSet, string realFile, string xpath, XmlNode new_node)
		{
			foreach (DocSetChangeset dscs in DocSetChangesets) {
				if (dscs.DocSet != docSet) 
					continue;
			
				foreach (FileChangeset fcs in dscs.FileChangesets) {
					if (fcs.RealFile != realFile)
						continue;
					
					foreach (Change c in fcs.Changes) {
						if (c.XPath == xpath) {
							c.NewNode = new_node;
							return;
						}
					}
					
					Change new_change = new Change (); {
						new_change.XPath = xpath;
						new_change.NewNode = new_node;
					}
					fcs.Changes.Add (new_change);
					return;
					
				}
				
				FileChangeset new_file_change_set = new FileChangeset (); {
					new_file_change_set.RealFile = realFile;
					
					Change new_change = new Change ();
					new_change.XPath = xpath;
					new_change.NewNode = new_node;
					new_file_change_set.Changes.Add (new_change);
				}
				
				dscs.FileChangesets.Add (new_file_change_set);
				return;
					
			}
			
			DocSetChangeset new_dcs = new DocSetChangeset (); {
				new_dcs.DocSet = docSet;
				
				FileChangeset new_file_change_set = new FileChangeset ();
				new_file_change_set.RealFile = realFile;
				
				Change new_change = new Change ();
				new_change.XPath = xpath;
				new_change.NewNode = new_node;
				new_file_change_set.Changes.Add (new_change);
				new_dcs.FileChangesets.Add (new_file_change_set);
			}
			DocSetChangesets.Add (new_dcs);
		}
	}
	
	public class DocSetChangeset {
		[XmlAttribute] public string DocSet;
		
		[XmlElement ("FileChangeset", typeof (FileChangeset))]
		public ArrayList FileChangesets = new ArrayList ();
	}
	
	public class FileChangeset {
		[XmlAttribute] public string RealFile;
		
		[XmlElement ("Change", typeof (Change))]
		public ArrayList Changes = new ArrayList ();
	}
	
	public class Change {
		[XmlAttribute] public string XPath;
		public XmlNode NewNode;

		public int Serial;
	}
#endregion
	
	public class EditMerger {
		GlobalChangeset changeset;
		ArrayList targetDirs;
		
		public EditMerger (GlobalChangeset changeset, ArrayList targetDirs)
		{
			this.changeset = changeset;
			this.targetDirs = targetDirs;
		}
		
		public void Merge ()
		{
			foreach (DocSetChangeset dsc in changeset.DocSetChangesets) {
				bool merged = false;
				foreach (string path in targetDirs) {
					if (File.Exists (Path.Combine (path, dsc.DocSet + ".source"))) {
						Merge (dsc, path);
						merged = true;
						break;
					}
				}
				if (!merged) Console.WriteLine ("Could not merge docset {0}", dsc.DocSet);
			}
		}
		
		void Merge (DocSetChangeset dsc, string path)
		{
			Console.WriteLine ("Merging changes in {0} ({1})", dsc.DocSet, path);
			
			foreach (FileChangeset fcs in dsc.FileChangesets) {
				if (File.Exists (Path.Combine (path, fcs.RealFile)))
					Merge (fcs, path);
				else
					Console.WriteLine ("\tCould not find file {0}", Path.Combine (path, fcs.RealFile));
			}
		}
		
		void Merge (FileChangeset fcs, string path)
		{
			XmlDocument d = new XmlDocument ();
			d.Load (Path.Combine (path, fcs.RealFile));
			
			foreach (Change c in fcs.Changes) {
				XmlNode old = d.SelectSingleNode (c.XPath);
				if (old != null)
					old.ParentNode.ReplaceChild (d.ImportNode (c.NewNode, true), old);
			}
			
			d.Save (Path.Combine (path, fcs.RealFile));
		}
	}
}
