//
// browser.cs: Mono documentation browser
//
// Author:
//   Miguel de Icaza
// Hacked apart by Chris Toshok for use as an evolution component.
//
// (C) 2003 Ximian, Inc.
//
// TODO:
//   Add support for printing.
//   Add back/forward buttons.
//   Add search facility
//   Add Index
//
using Gtk;
using GtkSharp;
using Glade;
using System;
using System.IO;
using System.Reflection;
using System.Collections;
using System.Web.Services.Protocols;
using System.Xml;

namespace EvoMonodoc {
public class Browser : Evolution.Component {
	Style bar_style;

	TreeView reference_tree;
	ScrolledWindow html_container;

  	Label subtitle_label;
    	Notebook nb;

	Box view_box;
	Box title_label_box;
	ELabel title_label;

	Gdk.Pixbuf monodoc_pixbuf;

	// Where we render the contents
	HTML html;

	// Our HTML preview during editing.
	HTML html_preview;

        //
	// Left-hand side Browsers
	//
	ScrolledWindow tree_container;
	TreeBrowser tree_browser;
	
	internal RootTree help_tree;

	// For the status bar.
	uint context_id;

        public override void CreateControls (out IntPtr sidebar, out IntPtr view)
	{
		sidebar = tree_container.Handle;
		view = view_box.Handle;
	}

	public void Activate ()
	{
		ActivateUI();
	}

	public void Deactivate ()
	{
		DeactivateUI();
	}

	void MakeSidebar ()
	{
		tree_container = new ScrolledWindow (null, null);

		tree_container.HscrollbarPolicy = PolicyType.Automatic;
		tree_container.VscrollbarPolicy = PolicyType.Automatic;

		help_tree = RootTree.LoadTree ();
		reference_tree = new TreeView ();
		reference_tree.HeadersVisible = false;

		tree_container.Add (reference_tree);
		reference_tree.Show();

		tree_browser = new TreeBrowser (help_tree, reference_tree, this);

		tree_container.Show();
	}

	void MakeView ()
	{
	  view_box = new VBox (false, 0);

	  title_label_box = new HBox (false, 6);
	  view_box.PackStart (title_label_box, false, false, 0);
	  title_label_box.Show();

	  //ellipsizing label for the title
	  title_label = new ELabel ("");
	  title_label.Xalign = 0;
	  Pango.FontDescription fd = new Pango.FontDescription ();
	  fd.Weight = Pango.Weight.Bold;
	  title_label.ModifyFont (fd);
	  title_label.Layout.FontDescription = fd;
	  title_label_box.PackStart (title_label, false, false, 0);
	  title_label.Show ();

	  subtitle_label = new Label ("");
	  title_label_box.PackStart (subtitle_label, false, false, 0);
	  subtitle_label.Show();

	  html_container = new ScrolledWindow (null, null);
	  html_container.HscrollbarPolicy = PolicyType.Automatic;
	  html_container.VscrollbarPolicy = PolicyType.Automatic;
	  view_box.PackStart (html_container, true, true, 0);
	  html_container.Show();

	  //
	  // Setup the HTML rendering area
	  //
	  html = new HTML ();
	  html.Show ();
	  html_container.Add (html);
	  html.LinkClicked += new LinkClickedHandler (LinkClicked);
	  html.OnUrl += new OnUrlHandler (OnUrlMouseOver);
	  html.UrlRequested += new UrlRequestedHandler (UrlRequested);

	  view_box.Show();
	}

	public Browser ()
	{
		Application.Init();

		MakeSidebar();
		MakeView();

		SetUI (null, "browser-ui.xml");
	}

	void BarStyleSet (object obj, StyleSetArgs args)
	{
		//bar_style.SetBackgroundGC (StateType.Normal, MainWindow.Style.BackgroundGCs[1]);
	}

	Stream GetResourceImage (string name)
	{
		Assembly assembly = System.Reflection.Assembly.GetCallingAssembly ();
		System.IO.Stream s = assembly.GetManifestResourceStream (name);
		
		return s;
	}

	void UrlRequested (object sender, UrlRequestedArgs args)
	{
		Console.WriteLine ("Image requested: " + args.Url);
		Stream s = help_tree.GetImage (args.Url);
		
		if (s == null)
			s = GetResourceImage ("monodoc.png");
		byte [] buffer = new byte [8192];
		int n;
		
		while ((n = s.Read (buffer, 0, 8192)) != 0) {
			args.Handle.Write (buffer, n);
		}
		args.Handle.Close (HTMLStreamStatus.Ok);
	}
	
	public class LinkPageVisit : PageVisit {
		Browser browser;
		string url;
		
		public LinkPageVisit (Browser browser, string url)
		{
			this.browser = browser;
			this.url = url;
		}

		public override void Go ()
		{
			Node n;
			
			string res = browser.help_tree.RenderUrl (url, out n);
			browser.Render (res, n, url);
		}
	}
	
        void LinkClicked (object o, LinkClickedArgs args)
	{
		LoadUrl (args.Url);
	}

        private System.Xml.XmlNode edit_node;
        private string edit_url;

	public void LoadUrl (string url)
	{
		if (url.StartsWith("#"))
		{
			// FIXME: This doesn't deal with whether anchor jumps should go in the history
			html.JumpToAnchor(url.Substring(1));
			return;
		}

		Node node;
		
		Console.Error.WriteLine ("Trying: {0}", url);
		string res = help_tree.RenderUrl (url, out node);
		if (res != null){
			Render (res, node, url);
			return;
		}
		
		Console.Error.WriteLine ("+----------------------------------------------+");
		Console.Error.WriteLine ("| Here we should locate the provider for the   |");
		Console.Error.WriteLine ("| link.  Maybe using this document as a base?  |");
		Console.Error.WriteLine ("| Maybe having a locator interface?   The short|");
		Console.Error.WriteLine ("| urls are not very useful to locate types     |");
		Console.Error.WriteLine ("+----------------------------------------------+");
		Render (url, null, url);
	}

	//
	// Renders the HTML text in `text' which was computed from `url'.
	// The Node matching is `matched_node'.
	//
	// `url' is only used for debugging purposes
	//
	public void Render (string text, Node matched_node, string url)
	{

		Gtk.HTMLStream stream = html.Begin ("text/html");

		stream.Write ("<html><body>");
		stream.Write (text);
		stream.Write ("</body></html>");
		html.End (stream, HTMLStreamStatus.Ok);
		if (matched_node != null) {
			if (tree_browser.SelectedNode != matched_node)
				tree_browser.ShowNode (matched_node);

			title_label.Text = matched_node.Caption;

			if (matched_node.Nodes != null) {
				int count = matched_node.Nodes.Count;
				string term;

				if (count == 1)
					term = "subpage";
				else
					term = "subpages";

				subtitle_label.Text = count + " " + term;
			} else
				subtitle_label.Text = "";
		} else {
			title_label.Text = "Error";
			subtitle_label.Text = "";
		}
	}
	
	//
	// Invoked when the mouse is over a link
	//
	string last_url = "";
	void OnUrlMouseOver (object o, OnUrlArgs args)
	{
	  //		string new_url = args.Url;
	  //
	  //		if (new_url == null)
	  //			new_url = "";
	  //		
	  //		if (new_url != last_url){
	  //			statusbar.Pop (context_id);
	  //			statusbar.Push (context_id, new_url);
	  //			last_url = new_url;
	  //		}
	}
	
	
	void OnCollapseActivate (object o, EventArgs args)
	{
		reference_tree.CollapseAll ();
		reference_tree.ExpandRow (new TreePath ("0"), false);
	}

	//
	// For the accel keystroke
	//
	void OnIndexEntryFocused (object sender, EventArgs a)
	{
		nb.Page = 1;
	}

	//
	// Invoked from File/Quit menu entry.
	//
	void OnQuitActivate (object sender, EventArgs a)
	{
		Application.Quit ();
	}

	//
	// Invoked by Edit/Copy menu entry.
	//
	void OnCopyActivate (object sender, EventArgs a)
	{
		html.Copy ();
	}

	class About {
		[Glade.Widget] Window about;
		[Glade.Widget] Image logo_image;

		static About AboutBox;
		Browser parent;
		
		About (Browser parent)
		{
			Glade.XML ui = new Glade.XML (null, "browser.glade", "about", null);
			ui.Autoconnect (this);
			this.parent = parent;

			//			about.TransientFor = parent.window1;

			logo_image.Pixbuf = new Gdk.Pixbuf (null, "monodoc.png");
		}

		void OnOkClicked (object sender, EventArgs a)
		{
			about.Hide ();
		}

		static public void Show (Browser parent)
		{
			if (AboutBox == null)
				AboutBox = new About (parent);
			AboutBox.about.Show ();
		}
	}

	//
	// Hooked up from Glade
	//
	void OnAboutActivate (object sender, EventArgs a)
	{
		About.Show (this);
	}


	//
	// Invoked by Edit/Select All menu entry.
	//
	void OnSelectAllActivate (object sender, EventArgs a)
	{
		html.SelectAll ();
	}
}

//
// This class implements the tree browser
//
class TreeBrowser {
	Browser browser;

	TreeView tree_view;
	
	TreeStore store;
	RootTree help_tree;
	TreeIter root_iter;

	//
	// This hashtable maps an iter to its node.
	//
	Hashtable iter_to_node;

	//
	// This hashtable maps the node to its iter
	//
	Hashtable node_to_iter;

	//
	// Maps a node to its TreeIter parent
	//
	Hashtable node_parent;

	public TreeBrowser (RootTree help_tree, TreeView reference_tree, Browser browser)
	{
		this.browser = browser;
		tree_view = reference_tree;
		iter_to_node = new Hashtable ();
		node_to_iter = new Hashtable ();
		node_parent = new Hashtable ();

		// Setup the TreeView
		tree_view.AppendColumn ("name_col", new CellRendererText (), "text", 0);

		// Bind events
		tree_view.RowExpanded += new GtkSharp.RowExpandedHandler (RowExpanded);
		tree_view.Selection.Changed += new EventHandler (RowActivated);

		// Setup the model
		this.help_tree = help_tree;
		store = new TreeStore (typeof (string));

		root_iter = store.AppendValues ("Mono Documentation");
		iter_to_node [root_iter] = help_tree;
		node_to_iter [help_tree] = root_iter;
		PopulateNode (help_tree, root_iter);

		reference_tree.Model = store;
	}

	void PopulateNode (Node node, TreeIter parent)
	{
		if (node.Nodes == null)
			return;

		TreeIter iter;
		foreach (Node n in node.Nodes){
			iter = store.AppendValues (parent, n.Caption);
			iter_to_node [iter] = n;
			node_to_iter [n] = iter;
		}
	}

	Hashtable populated = new Hashtable ();
	
	void RowExpanded (object o, GtkSharp.RowExpandedArgs args)
	{
		Node result = iter_to_node [args.Iter] as Node;

		Open (result);
	}

	void Open (Node node)
	{
		if (node == null){
			Console.Error.WriteLine ("Expanding something that I do not know about");
			return;
		}

		if (populated.Contains (node))
			return;
		
		//
		// We need to populate data on a second level
		//
		if (node.Nodes == null)
			return;

		foreach (Node n in node.Nodes){
			PopulateNode (n, (TreeIter) node_to_iter [n]);
		}
		populated [node] = true;
	}
	
	void PopulateTreeFor (Node n)
	{
		if (populated [n] == null){
			if (n.Parent != null) {
				OpenTree (n.Parent);
			}
		} 
		Open (n);
	}

	public void OpenTree (Node n)
	{
		PopulateTreeFor (n);

		TreeIter iter = (TreeIter) node_to_iter [n];
		TreePath path = store.GetPath (iter);
	}

	public Node SelectedNode
	{
		get {
	                Gtk.TreeIter iter = new Gtk.TreeIter ();
	                Gtk.TreeModel model;

	                if (tree_view.Selection.GetSelected (out model, ref iter))
	                        return (Node) iter_to_node [iter];
			else
				return null;
		}
	}
	
	public void ShowNode (Node n)
	{
		if (node_to_iter [n] == null){
			OpenTree (n);
			if (node_to_iter [n] == null){
				Console.Error.WriteLine ("Internal error: no node to iter mapping");
				return;
			}
		}
		
		TreeIter iter = (TreeIter) node_to_iter [n];
		TreePath path = store.GetPath (iter);

		//tree_view.ExpandToPath (path);

		IgnoreRowActivated = true;
		tree_view.Selection.SelectPath (path);
		IgnoreRowActivated = false;
		tree_view.ScrollToCell (path, null, true, 0.5f, 0.0f);
	}
	
	class NodePageVisit : PageVisit {
		Browser browser;
		Node n;
		string url;

		public NodePageVisit (Browser browser, Node n, string url)
		{
			if (n == null)
				throw new Exception ("N is null");
			
			this.browser = browser;
			this.n = n;
			this.url = url;
		}

		public override void Go ()
		{
			string res;
			Node x;
			
			// The root tree has no help source
			if (n.tree.HelpSource != null)
				res = n.tree.HelpSource.GetText (url, out x);
			else
				res = ((RootTree)n.tree).RenderUrl (url, out x);

			browser.Render (res, n, url);
		}
	}

	bool IgnoreRowActivated = false;
	
	//
	// This has to handle two kinds of urls: those encoded in the tree
	// file, which are used to quickly lookup information precisely
	// (things like "ecma:0"), and if that fails, it uses the more expensive
	// mechanism that walks trees to find matches
	//
	void RowActivated  (object sender, EventArgs a)
	{
		if (IgnoreRowActivated)
			return;
		
		Gtk.TreeIter iter = new Gtk.TreeIter ();
		Gtk.TreeModel model;

		if (tree_view.Selection.GetSelected (out model, ref iter)){
			Node n = (Node) iter_to_node [iter];
			
			string url = n.URL;
			Node match;
			string s;

			if (n.tree.HelpSource != null)
			{
				//
				// Try the tree-based urls first.
				//
				
				s = n.tree.HelpSource.GetText (url, out match);
				if (s != null){
					((Browser)browser).Render (s, n, url);
					return;
				}
			}
			
			//
			// Try the url resolver next
			//
			s = help_tree.RenderUrl (url, out match);
			if (s != null){
				((Browser)browser).Render (s, n, url);
				return;
			}

			((Browser)browser).Render ("<h1>Unhandled URL</h1>" + "<p>Functionality to view the resource <i>" + n.URL + "</i> is not available on your system or has not yet been implemented.</p>", null, url);
		}
	}

	public IntPtr Handle
	{
		get{
			return tree_view.Handle;
		}
	}
}

}
