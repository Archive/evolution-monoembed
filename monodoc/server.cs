//
// Monodoc server
//
// Author:
//   Miguel de Icaza (miguel@ximian.com)
//

using System;
using System.IO;
using System.Web.Mail;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Data;
using ByteFX.Data.MySqlClient;
using System.Xml;

namespace EvoMonodoc {
	public class Contributions : System.Web.Services.WebService
	{
		const string basedir = "/home/miguel/contributions/";
		//const string basedir = "/tmp/contributions/";

                private IDbConnection GetConnection() 
                {
			return new MySqlConnection("Server=erandi.boston.ximian.com; database=monodoc; " +
                                                          "user id=admin; Password=clave;");

                       // return new MySqlConnection("Server=localhost; database=test; user id=gonz; password=");
                }

                private MySqlParameter CreateParameter(string name, object value)
                {
                        return new MySqlParameter (name, value);
                }

		static void mail (string recipient, string body)
		{
			MailMessage m = new MailMessage ();
			m.From = "mono-docs-list@ximian.com";
			m.To = recipient;
			m.Subject = "Your Monodoc passkey";
			m.Body = String.Format ("\n\nWelcome to the Mono Documentation Effort,\n\n" + 
						"This is your passkey for contributing to the Mono Documentation effort:\n " +
						"       {0}\n\n" +
						"The Mono Documentation Team (mono-docs-list@ximian.com)", body);
			
			SmtpMail.SmtpServer = "localhost";
			SmtpMail.Send (m);
		}

		//
		// 0  => OK to send contributions.
		// -1 => Invalid version
		//
		[WebMethod]
		public int CheckVersion (int version)
		{
			if (version == 0)
				return 0;
			return -1;
		}
		
		//
		// Return codes:
		//    -3 invalid characters in login
		//    -2 Login already registered, password resent.
		//    -1 Generic error
		//     0 password mailed
		//
		[WebMethod]
		public int Register (string login)
		{
                        IDbConnection conn = GetConnection();
                        try 
                        {
				if (login.IndexOf ("'") != -1)
					return -3;
				
                                conn.Open();
                                IDbCommand cmd = conn.CreateCommand();
                                cmd.CommandText = String.Format ("select password from person where name='{0}'", login);
				IDataReader reader = cmd.ExecuteReader ();

				if (reader.Read ()){
					string password = (string) reader ["password"];
					Console.WriteLine ("Password for {0} is {1}", login, password);
					mail (login, password);
					reader.Close ();
					return -2;
				}
				reader.Close ();

				Random rnd = new Random ();
				int pass = rnd.Next ();
				string values = String.Format ("('{0}', '{1}', 0, 0)", login, pass);
				cmd.CommandText = "INSERT INTO person (name, password, serial_id, last_serial) VALUES " +
						  values;

				Console.WriteLine ("Password for {0} is {1}", login, pass);
				cmd.ExecuteNonQuery ();
				mail (login, pass.ToString ());
				
				return 0;
			} catch (Exception e) { Console.WriteLine (e);}
			finally {
				conn.Close ();
			}
			return -1;
		}
			
		[WebMethod]
		public int GetSerial (string login, string password)
		{
                        IDbConnection conn = GetConnection();
                        try 
                        {
                                conn.Open();
                                IDbCommand cmd = conn.CreateCommand();
                                cmd.CommandText = 
                                        String.Format ("select last_serial from person where name='{0}' and password='{1}'", login, password);
				Console.WriteLine (cmd.CommandText);
                                object r = cmd.ExecuteScalar();
				if (r != null){
					Console.WriteLine (r);
					return (int) r;
				}
                                return -1;
                        } catch (Exception e){
				Console.WriteLine ("Hola" + e);
			} finally {
                                conn.Close();
                        }
                        return -1;
  		}

		[WebMethod]
		public int Submit (string login, string password, XmlNode node)
		{
                        IDbConnection conn = GetConnection();

                        conn.Open();
                        IDbCommand cmd = conn.CreateCommand();
                        cmd.CommandText = 
                                String.Format ("select last_serial from person where name='{0}' and password='{1}'", login, password);
                        IDataReader reader = cmd.ExecuteReader (CommandBehavior.CloseConnection );

                        int ret_val = -1;

                        if (reader.Read())
                        {
                        
                                int id = (int)reader["serial_id"]; 
                                int serial = (int)reader["last_serial"]; 
                                reader.Close ();
                                
                                XmlDocument d = new XmlDocument ();
                                d.AppendChild (d.ImportNode (node, true));

                                string dudebase = basedir + id;
                                Directory.CreateDirectory (dudebase);
                                
                                d.Save (dudebase + "/" + serial + ".xml");

                                IDbTransaction txn = conn.BeginTransaction();
                                try 
                                {
                                        cmd.CommandText = "UPDATE person SET last_serial=@last_serial WHERE name=@name AND password=@pwd";
                                        cmd.Parameters.Add( CreateParameter("@last_serial", serial+1));
                                        cmd.Parameters.Add( CreateParameter("@name", login));
                                        cmd.Parameters.Add( CreateParameter("@pwd", password));
                                        cmd.ExecuteNonQuery ();

                                        // ... do more stuff ...

                                        txn.Commit();
                                } catch {
                                        txn.Rollback();
                                } finally {
					conn.Close();
				}

                                ret_val = serial+1;

				return ret_val;
			}
			return -1;
		}
	}
}
