//
// The provider for a tree of ECMA documents
//
// Authors:
//   Miguel de Icaza (miguel@ximian.com)
//   Joshua Tauberer (tauberer@for.net)
//
// (C) 2002, 2003 Ximian, Inc.
// (C) 2003 Joshua Tauberer.
//
// TODO:
//   Should cluster together constructors
//
// Easy:
//   Should render attributes on the signature.
//   Include examples as well.
//
namespace EvoMonodoc {
using System;
using System.Reflection;
using System.IO;
using System.Xml;
using System.Xml.XPath;
using System.Xml.Xsl;
using System.Text;
using System.Collections;
using ICSharpCode.SharpZipLib.Zip;

using BF = System.Reflection.BindingFlags;

//
// Helper routines to extract information from an Ecma XML document
//
public class EcmaDoc {
	public static string GetFullClassName (XmlDocument doc)
	{
		return doc.SelectSingleNode ("/Type").Attributes ["FullName"].InnerText;
	}
	
	public static string GetClassName (XmlDocument doc)
	{
		return doc.SelectSingleNode ("/Type").Attributes ["Name"].InnerText;
	}

	public static string GetClassAssembly (XmlDocument doc)
	{
		return doc.SelectSingleNode ("/Type/AssemblyInfo/AssemblyName").InnerText;
	}

	public static string GetClassNamespace (XmlDocument doc)
	{
		string s = doc.SelectSingleNode ("/Type").Attributes ["FullName"].InnerText;

		return s.Substring (0, s.LastIndexOf ("."));
	}
	
	public static string GetTypeKind (XmlDocument doc)
	{
		XmlNode node = doc.SelectSingleNode ("/Type/Base/BaseTypeName");

		if (node == null){
			if (GetFullClassName (doc) == "System.Object")
				return "Class";
			return "Interface";
		}

		switch (node.InnerText){

		case "System.Delegate":
			return "Delegate";
		case "System.ValueType":
			return "Structure";
		case "System.Enum":
			return "Enumeration";
		default:
			return "Class";
		}
	}

	//
	// Utility function: converts a fully .NET qualified type name into a C#-looking one
	//
	public static string ConvertCTSName (string type)
	{
		if (!type.StartsWith ("System."))
			return type;

		if (type.EndsWith ("*"))
			return ConvertCTSName(type.Substring(0, type.Length - 1)) + "*";
		if (type.EndsWith ("&"))
			return ConvertCTSName(type.Substring(0, type.Length - 1)) + "&";
		if (type.EndsWith ("[]"))
			return ConvertCTSName(type.Substring(0, type.Length - 2)) + "[]";

		switch (type) {
		case "System.Byte": return "byte";
		case "System.SByte": return "sbyte";
		case "System.Int16": return "short";
		case "System.Int32": return "int";
		case "System.Int64": return "long";
			
		case "System.UInt16": return "ushort";
		case "System.UInt32": return "uint";
		case "System.UInt64": return "ulong";
			
		case "System.Single":  return "float";
		case "System.Double":  return "double";
		case "System.Decimal": return "decimal";
		case "System.Boolean": return "bool";
		case "System.Char":    return "char";
		case "System.String":  return "string";
			
		case "System.Object":  return "object";
		case "System.Void":  return "void";
		}

		if (type.LastIndexOf(".") == 6)
			return type.Substring(7);
		
		return type;
	}
}

//
// The Ecma documentation provider:
//
// It populates a tree with contents during the help assembly
//
public class EcmaProvider : Provider {
	string basedir;

	public EcmaProvider (string base_directory)
	{
		basedir = base_directory;

		if (!Directory.Exists (basedir))
			throw new FileNotFoundException (String.Format ("The directory `{0}' does not exist", basedir));
	}
	
	public override void PopulateTree (Tree tree)
	{
		string [] namespaces = Directory.GetDirectories (basedir);
		

		
		foreach (string ns in namespaces){
			string [] files = Directory.GetFiles (ns);
			Node ns_node = null;
			string tn = null;

			foreach (string file in files){
				if (!file.EndsWith (".xml"))
					continue;

				if (ns_node == null){
					tn = Path.GetFileName (ns);
					Console.Error.WriteLine ("Processing namespace {0}", tn);
					ns_node = tree.LookupNode (tn, "N:" + tn);
					string ns_summary_file = basedir + "/" + tn + ".xml";
					

					
					if (File.Exists (ns_summary_file)) {
						XmlDocument nsSummaryFile = new XmlDocument ();
						nsSummaryFile.Load (ns_summary_file);
						
						namespace_summaries [tn] = nsSummaryFile.SelectSingleNode ("Namespace/Docs/summary");
						namespace_remarks [tn] = nsSummaryFile.SelectSingleNode ("Namespace/Docs/remarks");
						
					} else if (!namespace_summaries.ContainsKey (tn)) {
						namespace_summaries [tn] = null;
						namespace_remarks [tn] = null;
					}
				}
				Console.Error.WriteLine ("    Processing input file {0}", Path.GetFileName (file));

				PopulateClass (tn, ns_node, file);
			}
		}

	}
		
	struct TypeInfo : IComparable {
		public string type_assembly;
		public string type_name;
		public string type_full;
		public string type_kind;
		public XmlNode type_doc;

		public TypeInfo (string k, string a, string f, string s, XmlNode n)
		{
			type_assembly = a;
			type_name = s;
			type_doc = n;
			type_kind = k;
			type_full = f;
		}

		public int CompareTo (object b)
		{
			TypeInfo na = this;
			TypeInfo nb = (TypeInfo) b;

			return String.Compare (na.type_full, nb.type_full);
		}
	}
	
	//
	// Packs a file with the summary data
	//
	public override void CloseTree (HelpSource hs, Tree tree)
	{
		foreach (DictionaryEntry de in class_summaries){
			XmlDocument doc = new XmlDocument ();
			string ns = (string) de.Key;
			
			ArrayList list = (ArrayList) de.Value;
			list.Sort();

			XmlElement elements = doc.CreateElement ("elements");
			doc.AppendChild (elements);
			
			if (namespace_summaries [ns] != null)
				elements.AppendChild (doc.ImportNode ((XmlNode)namespace_summaries [ns],true));
			else
				elements.AppendChild (doc.CreateElement("summary"));
			
			if (namespace_remarks [ns] != null)
				elements.AppendChild (doc.ImportNode ((XmlNode)namespace_remarks [ns],true));
			else
				elements.AppendChild (doc.CreateElement("remarks"));
			
			Console.Error.WriteLine ("Have {0} elements in the {1}", list.Count, ns);
			foreach (TypeInfo p in list){
				XmlElement e = null;
				
				switch (p.type_kind){
				case "Class":
					e = doc.CreateElement ("class"); 
					break;
					
				case "Enumeration":
					e = doc.CreateElement ("enum");
					break;
					
				case "Structure":
					e = doc.CreateElement ("struct");
					break;
					
				case "Delegate":
					e = doc.CreateElement ("delegate");
					break;
					
				case "Interface":
					e = doc.CreateElement ("interface");
					break;
				}
				
				e.SetAttribute ("name", p.type_name);
				e.SetAttribute ("fullname", p.type_full);
				e.SetAttribute ("assembly", p.type_assembly);
				XmlNode copy = doc.ImportNode (p.type_doc, true);
				e.AppendChild (copy);
				elements.AppendChild (e);
			}
			hs.PackXml ("xml.summary." + ns, doc);
		}
		
		
		XmlDocument nsSummary = new XmlDocument ();
		XmlElement root = nsSummary.CreateElement ("elements");
		nsSummary.AppendChild (root);
		
		foreach (DictionaryEntry de in namespace_summaries) {
			XmlNode n = (XmlNode)de.Value;
			XmlElement summary = nsSummary.CreateElement ("namespace");
			summary.SetAttribute ("ns", (string)de.Key);
			root.AppendChild (summary);
			if (n != null)
				summary.AppendChild (nsSummary.ImportNode (n,true));
			else
				summary.AppendChild (nsSummary.CreateElement("summary"));
			
		}
		tree.HelpSource.PackXml ("mastersummary.xml", nsSummary);
	}
	       
	static Hashtable class_summaries = new Hashtable ();
	static Hashtable namespace_summaries = new Hashtable ();
	static Hashtable namespace_remarks = new Hashtable ();
	XmlDocument doc;
	
	void PopulateClass (string ns, Node ns_node, string file)
	{
		doc = new XmlDocument ();
		doc.Load (file);
		
		string name = EcmaDoc.GetClassName (doc);
		string assembly = EcmaDoc.GetClassAssembly (doc);
		string kind = EcmaDoc.GetTypeKind (doc);
		string full = EcmaDoc.GetFullClassName (doc);

		Node class_node;
		string file_code = ns_node.tree.HelpSource.PackFile (file);

		XmlNode class_summary = doc.SelectSingleNode ("/Type/Docs/summary");
		ArrayList l = (ArrayList) class_summaries [ns];
		if (l == null){
			l = new ArrayList ();
			class_summaries [ns] = (object) l;
		}
		l.Add (new TypeInfo (kind, assembly, full, name, class_summary));
	       
		class_node = ns_node.LookupNode (String.Format ("{0} {1}", name, kind), "ecma:" + file_code + "#" + name + "/");
		
		if (kind == "Delegate") {
			if (doc.SelectSingleNode("/Type/ReturnValue") == null)
				Console.Error.WriteLine("Delegate " + name + " does not have a ReturnValue node.  See the ECMA-style updates.");
		}

		if (kind == "Enumeration")
			return;

		if (kind == "Delegate")
			return;
		
		//
		// Always add the Members node
		//
		class_node.CreateNode ("Members", "*");

		PopulateMember (name, class_node, "Constructor", "Constructors");
		PopulateMember (name, class_node, "Method", "Methods");
		PopulateMember (name, class_node, "Property", "Properties");
		PopulateMember (name, class_node, "Field", "Fields");
		PopulateMember (name, class_node, "Event", "Events");
	}

	class NodeIndex {
		public XmlNode node;
		public int     index;

		public NodeIndex (XmlNode node, int index)
		{
			this.node = node;
			this.index = index;
		}
	}

	struct NodeCompare : IComparer {
		public int Compare (object a, object b)
		{
			NodeIndex na = (NodeIndex) a;
			NodeIndex nb = (NodeIndex) b;

			return String.Compare (na.node.Attributes ["MemberName"].InnerText,
					       nb.node.Attributes ["MemberName"].InnerText);
		}
	}

	static NodeCompare NodeComparer = new NodeCompare ();

	string GetMemberName (XmlNode node)
	{
		return node.Attributes ["MemberName"].InnerText;
	}
	
	//
	// Performs an XPath query on the document to extract the nodes for the various members
	// we also use some extra text to pluralize the caption
	//
	void PopulateMember (string typename, Node node, string text, string plural_text)
	{
		XmlNodeList list = doc.SelectNodes (String.Format ("/Type/Members/Member[MemberType=\"{0}\"]", text));
		int count = list.Count;
		
		if (count == 0)
			return;

		Node nodes_node;
		string key = text.Substring (0, 1);
		if (count == 1)
			nodes_node = node.CreateNode (text, key);
		else
			nodes_node = node.CreateNode (plural_text, key);
		
		int i = 0;

		
		switch (text) {
			case "Event":
			case "Property":
			case "Field":
				foreach (XmlNode n in list)
					nodes_node.CreateNode (GetMemberName (n), (i++).ToString ());
				break;

			case "Constructor":
				foreach (XmlNode n in list)
					nodes_node.CreateNode (EcmaHelpSource.MakeSignature(n, typename), (i++).ToString ());
				break;

			case "Method":
				foreach (XmlNode n in list) {
					bool multiple = false;
					foreach (XmlNode nn in list) {
						if (n != nn && n.Attributes ["MemberName"].InnerText == nn.Attributes ["MemberName"].InnerText) {
							multiple = true;
							break;
						}
					}
					
					if (multiple) {
						nodes_node.LookupNode (n.Attributes ["MemberName"].InnerText, n.Attributes ["MemberName"].InnerText)
							.CreateNode (EcmaHelpSource.MakeSignature(n, null), (i++).ToString ());
					} else {
						nodes_node.CreateNode (n.Attributes ["MemberName"].InnerText, (i++).ToString ());
					}
				}
				
				foreach (Node n in nodes_node.Nodes) {
					if (!n.IsLeaf)
						n.Sort ();
				}
				
				break;
				
			default:
				foreach (XmlNode n in list)
				{
					string sig;
					XmlNode signode = n.SelectSingleNode ("MemberSignature[@Language='C#']");
				
					if (signode == null)
						sig = GetMemberName (n);
					else
						sig = signode.Attributes ["Value"].InnerText;
					
					nodes_node.CreateNode (sig, (i++).ToString ());
					
				}
				break;
		}
		
		nodes_node.Sort ();
	}

}

//
// The Ecma HelpSource provider
//
public class EcmaHelpSource : HelpSource {

	public EcmaHelpSource (string base_file, bool create) : base (base_file, create)
	{
	}

	public override string GetText (string url, out Node match_node)
	{
		match_node = null;
		
		if (url == "root:")
		{
			XmlReader summary = GetHelpXml ("mastersummary.xml");
			if (summary == null)
				return null;


			XsltArgumentList args = new XsltArgumentList();
			args.AddExtensionObject("monodoc://extensions", ExtObject);
			args.AddParam("show", "", "masteroverview");
			return Htmlize(new XPathDocument (summary), args);
		}
		
		if (url.StartsWith ("ecma:"))
			return GetTextFromUrl (url);

		return null;
	}


	string RenderMemberLookup (string typename, string member, ref Node type_node)
	{
		if (type_node.Nodes == null)
			return null;

		string lasturl = "";
		XmlNode doc = null;

		string membername = member;
		string[] argtypes = null;
		if (member.IndexOf("(") > 0) {
			membername = membername.Substring(0, member.IndexOf("("));
			member = member.Replace("@", "&");
			
			// reform the member signature with CTS names

			string x = member.Substring(member.IndexOf("(")+1);
			argtypes = x.Substring(0, x.Length-1).Split(',');

			if (membername == ".ctor")
				member = typename;
			else
				member = membername;

			member += "(";
			for (int i = 0; i < argtypes.Length; i++) {
				argtypes[i] = EcmaDoc.ConvertCTSName(argtypes[i]);
				if (i > 0) member += ",";
				member += argtypes[i];
			}
			member += ")";
		}

		// Check if a node caption matches exactly

		foreach (Node x in type_node.Nodes){
			if (x.Nodes == null) continue;
			foreach (Node m in x.Nodes) {
				if (m.Caption == member || m.Caption.StartsWith(member + "(")) {
					type_node = m;
					return GetTextFromUrl (m.URL);
				}
				
				if (!m.IsLeaf) {
					foreach (Node mm in m.Nodes) {
						if (mm.Caption == member || mm.Caption.StartsWith(member + "(")) {
							type_node = mm;
							return GetTextFromUrl (mm.URL);
						}
					}
				}
			}
		}
		
		// I have never seen this activated, and it now contains bugs because
		// of method grouping, so i am commenting it out. I would love if someone
		// explained why we have this...
#if false
		// Check the signatures within each document (sllloooow)
			
		foreach (Node x in type_node.Nodes){
			if (x.Nodes == null) continue;
			foreach (Node m in x.Nodes){
				if (m.Caption.StartsWith(membername) || membername == ".ctor" ||
				    (membername == "Finalize" && m.Caption.StartsWith("~")) ||
				    (membername.StartsWith("op_") && m.Caption.IndexOf("operator") >= 0) ||
				    m.Caption.IndexOf("this") >= 0) {

					string url = m.URL.Substring (5);

					string rest;
					string file = GetFile (url, out rest);

					if (url != lasturl) {
						doc = GetDocument (this, file).DocumentElement;
						lasturl = url;
					}

					string [] nodes = rest.Split (new char [] {'/'});
					int idx = int.Parse(nodes [1]);
					string nodetype = "";

					switch (nodes [0]){
					case "C": nodetype = "Constructor"; break;
					case "M": nodetype = "Method"; break;
					case "P": nodetype = "Property"; break;
					case "F": nodetype = "Field"; break;
					case "E": nodetype = "Event"; break;
					}

					XmlNode n = doc.SelectNodes("Members/Member[MemberType='" + nodetype + "']")[idx];

					if (n == null) continue;

					string sig = MakeSignature(n, nodes[0] == "C" ? typename : null);

					//Console.WriteLine(sig + " -- " + member);

					if (sig == member) {
						type_node = m;
						return GetTextFromUrl (m.URL);
					}
				}
			}
		}
#endif
		return null;
	}

	public static string MakeSignature(XmlNode n, string cstyleclass) {
		string sig;

		if (cstyleclass == null)
			sig = n.Attributes["MemberName"].InnerText;
		else // constructor style
			sig = cstyleclass;
	
		if (n.SelectSingleNode("MemberType").InnerText == "Method" || n.SelectSingleNode("MemberType").InnerText == "Constructor") {
			XmlNodeList paramnodes = n.SelectNodes("Parameters/Parameter");
			sig += "(";
			bool first = true;
			foreach (XmlNode p in paramnodes) {
				if (!first) sig += ",";
				string type = p.Attributes["Type"].InnerText;
				type = EcmaDoc.ConvertCTSName(type);
				sig += type;
				first = false;
			}
			sig += ")";
		}

		return sig;
	}
	
	//
	// This routine has to perform a lookup on a type.
	//
	// Example: T:System.Text.StringBuilder
	//
	// The prefix is the kind of opereation being requested (T:, E:, M: etc)
	// ns is the namespace being looked up
	// type is the type being requested
	//
	// This has to walk our toplevel (which is always a namespace)
	// And then the type space, and then walk further down depending on the request
	//
	public override string RenderTypeLookup (string prefix, string ns, string type, string member, out Node match_node)
	{
			
		// If a nested type, compare only inner type name to node list.
		// This should be removed when the node list doesn't lose the continaing type name.
		if (type.IndexOf("+") > 0) type = type.Substring(type.IndexOf("+")+1);

		string nsp = prefix + ns;

		foreach (Node ns_node in Tree.Nodes){
			string ns_node_namespace = ns_node.Element.Substring (2);

			if (ns_node_namespace != ns)
				continue;
			
			foreach (Node type_node in ns_node.Nodes){
				string element = type_node.Element;
				int pidx = element.IndexOf ("#");
				int sidx = element.IndexOf ("/");
				
				string cname = element.Substring (pidx + 1, sidx-pidx-1);
				
				//Console.WriteLine ("t:{0} cn:{1} p:{2}", type, cname, prefix);

				if (type == cname && prefix == "T:"){
					match_node = type_node;
					return GetTextFromUrl (type_node.URL);
				} else if (type.StartsWith (cname)){
					int p = cname.Length;

					match_node = type_node;
					if (type == cname){
						string ret = RenderMemberLookup (type, member, ref match_node);
						if (ret == null)
							return GetTextFromUrl (type_node.URL);
						return ret;

					} else if (type [p] == '/'){
						//
						// This handles summaries
						//
						match_node = null;
						foreach (Node nd in type_node.Nodes) {
							if (nd.Element [nd.Element.Length - 1] == type [p + 1]) {
								match_node = nd;
								break;
							}
						}
						
						return GetTextFromUrl (type_node.URL + type.Substring (p + 1));
					}
				}
			}
			match_node = null;
			return String.Format ("<b>FOUND!</b><p><p><b>Namespace: <b>{0}<br><b>Type:</b> {1}", ns, type);
		}

		match_node = null;
		return null;
	}


	public override string RenderNamespaceLookup (string nsurl, out Node match_node)
	{
		foreach (Node ns_node in Tree.Nodes){
			if (ns_node.Element != nsurl)
				continue;

			match_node = ns_node;
			string ns_name = nsurl.Substring (2);
			
			XmlReader summary = GetHelpXml ("xml.summary." + ns_name);
			if (summary == null)
				return null;

			XsltArgumentList args = new XsltArgumentList();
			args.AddExtensionObject("monodoc://extensions", ExtObject);
			args.AddParam("show", "", "namespace");
			args.AddParam("namespace", "", ns_name);
			return Htmlize(new XPathDocument (summary), args);

		}
		match_node = null;
		return null;
	}

							   
	string GetTextFromUrl (string url)
	{
		string file, rest;

		//
		// Remove ecma:
		//
		url = url.Substring (5);
		file = GetFile (url, out rest);

		// Console.WriteLine ("Got [{0}] and [{1}]", file, rest);
		XmlDocument doc = GetHelpXmlWithChanges (file);
		if (doc == null)
			return null;

		XsltArgumentList args = new XsltArgumentList();

		args.AddExtensionObject("monodoc://extensions", ExtObject);
		
		if (rest == "") {
			args.AddParam("show", "", "typeoverview");
			return Htmlize(doc, args);
		}
		
		string [] nodes = rest.Split (new char [] {'/'});
		
		switch (nodes.Length) {
			case 1:
				args.AddParam("show", "", "members");
				args.AddParam("index", "", "all");
				break;
			case 2:
				// Could either be a single member, or an overload thingy
				try {
					int dummy = int.Parse (nodes [1]); // is it an int
					
					args.AddParam("show", "", "member");
					args.AddParam("index", "", nodes [1]);
				} catch {
					args.AddParam("show", "", "overloads");
					args.AddParam("index", "", nodes [1]);
				}
				break;
			case 3:
				args.AddParam("show", "", "member");
				args.AddParam("index", "", nodes [2]);
				break;
			default:
				return "What the hell is this URL " + url;
		}

		switch (nodes [0]){
		case "C":
			args.AddParam("membertype", "", "Constructor");
			break;
		case "M":
			args.AddParam("membertype", "", "Method");
			break;
		case "P":
			args.AddParam("membertype", "", "Property");
			break;
		case "F":
			args.AddParam("membertype", "", "Field");
			break;
		case "E":
			args.AddParam("membertype", "", "Event");
			break;
		case "*":
			args.AddParam("membertype", "", "All");
			break;
		default:
			return "Unknown url: " + url;
		}

		return Htmlize(doc, args);
	}

	
	public override void RenderPreviewDocs (XmlNode newNode, XmlWriter writer)
	{
		XsltArgumentList args = new XsltArgumentList ();
		args.AddExtensionObject ("monodoc://extensions", ExtObject);
		
		Htmlize (newNode, args, writer);
	}

	static XslTransform ecma_transform;

	public static string Htmlize (IXPathNavigable ecma_xml)
	{
		return Htmlize(ecma_xml, null);
	}
	
	static string Htmlize (IXPathNavigable ecma_xml, XsltArgumentList args)
	{
		EnsureTransform ();
		
		StringWriter output = new StringWriter ();
		ecma_transform.Transform (ecma_xml, args, output);
		return output.ToString ();
	}
	
	static void Htmlize (IXPathNavigable ecma_xml, XsltArgumentList args, XmlWriter w)
	{

		EnsureTransform ();
		
		if (ecma_xml == null)
			return;

		ecma_transform.Transform (ecma_xml, args, w);
	}
	
	static void EnsureTransform ()
	{
		if (ecma_transform == null) {
			ecma_transform = new XslTransform ();
			Assembly assembly = System.Reflection.Assembly.GetCallingAssembly ();
			Stream stream = assembly.GetManifestResourceStream ("mono-ecma.xsl");

			XmlReader xml_reader = new XmlTextReader (stream);
			ecma_transform.Load (xml_reader);
		}
	}

	// This ExtensionObject stuff is used to check at run time whether
	// types and members have been implemented and whether there are any
	// MonoTODO attributes left on them. 

	private readonly ExtensionObject ExtObject = new ExtensionObject();
	private class ExtensionObject
	{
		// Used by stylesheet to nicely reformat the <see cref=> tags. 
		public string MakeNiceSignature(string sig)
		{
			if (sig.Length < 3)
				return sig;
			if (sig[1] != ':')
				return sig;

			char s = sig[0];
			sig = sig.Substring(2);

			switch (s) {
				case 'N': return sig;
				case 'T': return ShortTypeName(sig);

				case 'C': case 'M': case 'P': case 'F': case 'E':
					int paren = sig.IndexOf("(");
					if (paren > 0) {
						string name = sig.Substring(0, paren);
						string[] args = sig.Substring(paren, sig.Length-paren-1).Split(',');
						for (int i = 0; i < args.Length; i++)
							args[i] = ShortTypeName(args[i]);
						return ShortTypeName(name) + "(" + String.Join(", ", args) + ")";
					} else {
						return ShortTypeName(sig);
					}

				default:
					return sig;
			}
		}
		
		public string EditUrl (XPathNodeIterator itr)
		{
			if (itr.MoveNext ())
				return EditingUtils.GetEditUri (itr.Current);
			
			return "";
		}

		private static string ShortTypeName(string name)
		{
			if (name.LastIndexOf(".") > 0) name = name.Substring(name.LastIndexOf(".")+1);
			return name;
		}


		public string MonoImpInfo(string assemblyname, string typename, string membername, string arglist, bool strlong)
		{
			ArrayList a = new ArrayList();
			if (arglist != "") a.Add(arglist);
			return MonoImpInfo(assemblyname, typename, membername, a, strlong);
		}

		public string MonoImpInfo(string assemblyname, string typename, string membername, ArrayList arglist, bool strlong)
		{
			try {
				Assembly assembly = null;
				
				try {
					assembly = Assembly.Load(assemblyname);
				} catch (Exception e) { }
				if (assembly == null) {
					/*if (strlong) return "The assembly " + assemblyname + " is not available to MonoDoc.";
					else return "";*/
					return ""; // silently ignore
				}

				Type t = assembly.GetType(typename, false);
				if (t == null) {
					if (strlong)
						return typename + " has not been implemented.";
					else
						return "Not implemented.";
				}

				MemberInfo[] mis = t.GetMember(membername, BF.Static | BF.Instance | BF.Public | BF.NonPublic);

				if (mis.Length == 0)
					return "This member has not been implemented.";
				if (mis.Length == 1)
					return MonoImpInfo(mis[0], "member", strlong);

				// Scan for the appropriate member
				foreach (MemberInfo mi in mis) {
					System.Reflection.ParameterInfo[] pis;

					if (mi is MethodInfo || mi is ConstructorInfo)
						pis = ((MethodBase)mi).GetParameters();
					else if (mi is PropertyInfo)
						pis = ((PropertyInfo)mi).GetIndexParameters();
					else
						pis = null;
					
					if (pis != null) {
						bool good = true;
						if (pis.Length != arglist.Count) continue;
						for (int i = 0; i < pis.Length; i++) {
							if (pis[i].ParameterType.FullName != (string)arglist[i]) { good = false; break; }
						}
						if (!good) continue;
					}

					return MonoImpInfo(mi, "member", strlong);
				}

				return "This member has not been implemented.";

			} catch (Exception e) {
				return "An error occured while loading type information: " + e.Message;
			}
		}
		
		public string MonoImpInfo(System.Reflection.MemberInfo mi, string itemtype, bool strlong)
		{
			string s = "";

			object[] atts = mi.GetCustomAttributes(true);
			int todoctr = 0;
			foreach (object att in atts) if (att.GetType().Name == "MonoTODOAttribute") todoctr++;

			if (todoctr > 0) {
				if (strlong)
					s = "This " + itemtype + " is marked as being unfinished.<BR/>\n";
				else 
					s = "Unfinished.";
			}

			return s;
		}

		public string MonoImpInfo(string assemblyname, string typename, bool strlong)
		{
			try {
				if (assemblyname == "")
					return "";

				Assembly assembly = Assembly.Load(assemblyname);
				if (assembly == null) {
					if (strlong)
						return "The assembly " + assemblyname + " is not available to MonoDoc.";
					else
						return "";
				}

				Type t = assembly.GetType(typename, false);
				if (t == null) {
					if (strlong)
						return typename + " has not been implemented.";
					else
						return "Not implemented.";
				}

				string s = MonoImpInfo(t, "type", strlong);

				if (strlong) {
					MemberInfo[] mis = t.GetMembers(BF.Static | BF.Instance | BF.Public | BF.NonPublic);

					// Scan members for MonoTODO attributes
					int mctr = 0;
					foreach (MemberInfo mi in mis) {
						string mii = MonoImpInfo(mi, null, false);
						if (mii != "") mctr++; 
					}
					if (mctr > 0) {
						s += "This type has " + mctr + " members that are marked as unfinished.<BR/>";
					}
				}

				return s;

			} catch (Exception e) {
				return "An error occured while loading type information: " + e.Message;
			}			
		}

		public string GetInheritance(string assemblyname, string typename)
		{
			try {
				if (assemblyname == "")
					return "";
				Assembly assembly = Assembly.Load(assemblyname);
				if (assembly == null)
					return "";
				Type t = assembly.GetType(typename, false);
				if (t == null)
					return "";
				
				StringBuilder b = new StringBuilder();
				
				// Class inheritance

				ArrayList inh = new ArrayList();
				Type ti = t;
				while (ti != null) {
					inh.Add(ti.FullName);
					ti = ti.BaseType;
				}
				if (inh.Count > 1) {
					inh.Reverse();					
					for (int i = 0; i < inh.Count; i++) {
						for (int j = 0; j < i; j++)
							b.Append("&nbsp; &nbsp; &nbsp;");
						b.Append("<a href=\"T:"); b.Append(inh[i]); b.Append("\">");
						b.Append(inh[i]);
						b.Append("</a><br/>\n");
					}
					b.Append("<br/>\n");
				}
				
				return b.ToString();
			} catch (Exception e) {
				return "";
			}			
		}
		
		public bool MonoEditing ()
		{
			return SettingsHandler.Settings.EnableEditing && Settings.RunningGUI;
		}
	}

	//
	// This takes one of the ecma urls, which look like this:
	// ecma:NUMERIC_ID#OPAQUE/REST
	//
	// NUMERIC_ID is the numeric ID assigned by the compressor
	// OPAQUE is opaque for node rendering (it typically contains T:System.Byte for example)
	// REST is the rest of the argument used to decode information
	//
	static string GetFile (string url, out string rest)
	{
		int pound = url.IndexOf ("#");
		int slash = url.IndexOf ("/");
		
		string fname = url.Substring (0, pound);
		rest = url.Substring (slash+1);

		return fname;
	}

	// This should have a little cache or something.
	static XmlDocument GetDocument (HelpSource hs, string fname)
	{
		Stream s = hs.GetHelpStream (fname);
		if (s == null){
			Console.Error.WriteLine ("Could not fetch document {0}", fname);
			return null;
		}
		
		XmlDocument doc = new XmlDocument ();

		doc.Load (s);
		
		return doc;
	}

	string GetKindFromCaption (string s)
	{
		int p = s.LastIndexOf (' ');
		if (p > 0)
			return s.Substring (p + 1);
		return null;
	}
	
	//
	// Populates the index.
	//
	public override void PopulateIndex (IndexMaker index_maker)
	{
		foreach (Node ns_node in Tree.Nodes){
			foreach (Node type_node in ns_node.Nodes){
				string typename = type_node.Caption.Substring (0, type_node.Caption.IndexOf (' '));
				string full = ns_node.Caption + "." + typename;

				string doc_tag = GetKindFromCaption (type_node.Caption);

				if (doc_tag == "Class" || doc_tag == "Structure" || doc_tag == "Interface"){
					string url = "T:" + full;
					
					index_maker.Add (type_node.Caption, typename, url);
					index_maker.Add (full + " " + doc_tag, full, url);


					foreach (Node c in type_node.Nodes){
						switch (c.Caption){
						case "Constructors":
							index_maker.Add ("  constructors", typename+"0", url + "/C");
							break;
						case "Fields":
							index_maker.Add ("  fields", typename+"1", url + "/F");
							break;
						case "Events":
							index_maker.Add ("  events", typename+"2", url + "/E");
							break;
						case "Properties":
							index_maker.Add ("  properties", typename+"3", url + "/P");
							break;
						case "Methods":
							index_maker.Add ("  methods", typename+"4", url + "/M");
							break;
						}
					}

					//
					// Now repeat, but use a different sort key, to make sure we come after
					// the summary data above, start the counter at 5
					//
					string keybase = typename + "5.";
					
					foreach (Node c in type_node.Nodes){
						switch (c.Caption){
						case "Constructors":
							break;
						case "Fields":
							foreach (Node nc in c.Nodes){
								string res = nc.Caption;

								string nurl = String.Format ("F:{0}.{1}", full, res);
								index_maker.Add (String.Format ("{0}.{1} field", typename, res),
										 keybase + res, nurl);
								index_maker.Add (String.Format ("{0} field", res), res, nurl);
							}

							break;
						case "Events":
							foreach (Node nc in c.Nodes){
								string res = nc.Caption;
								string nurl = String.Format ("E:{0}.{1}", full, res);
								
								index_maker.Add (String.Format ("{0}.{1} event", typename, res),
										 keybase + res, nurl);
								index_maker.Add (String.Format ("{0} event", res), res, nurl);
							}
							break;
						case "Properties":
							foreach (Node nc in c.Nodes){
								string res = nc.Caption;
								string nurl = String.Format ("P:{0}.{1}", full, res);
								index_maker.Add (String.Format ("{0}.{1} property", typename, res),
										 keybase + res, nurl);
								index_maker.Add (String.Format ("{0} property", res), res, nurl);
							}
							break;
						case "Methods":
							foreach (Node nc in c.Nodes){
								string res = nc.Caption;
								int p = res.IndexOf ("(");
								if (p > 0)
									res = res.Substring (0, p); 
								string nurl = String.Format ("M:{0}.{1}", full, res);
								index_maker.Add (String.Format ("{0}.{1} method", typename, res),
										 keybase + res, nurl);
								index_maker.Add (String.Format ("{0} method", res), res, nurl);
							}
					
							break;
						}
					}
				}
			}
		}
	}
}
}
