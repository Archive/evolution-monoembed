//
// The ecmaspec provider is for ECMA specifications
//
// Authors:
//	John Luke (jluke@cfl.rr.com)
//	Ben Maurer (bmaurer@users.sourceforge.net)
//
// Use like this:
//   mono assembler.exe --ecmaspec DIRECTORY --out name
//

namespace EvoMonodoc {
using System;
using System.IO;
using System.Text;
using System.Xml.XPath;
using System.Xml.Xsl;
using System.Xml;

public class EcmaSpecProvider : Provider {
	string basedir;
	
	public EcmaSpecProvider (string base_directory)
	{
		basedir = base_directory;
		if (!Directory.Exists (basedir))
			throw new FileNotFoundException (String.Format ("The directory `{0}' does not exist", basedir));
	}
	
	public override void PopulateTree (Tree tree)
	{
		XPathNavigator n = new XPathDocument (Path.Combine (basedir, "toc.xml")).CreateNavigator ();
		n.MoveToRoot ();
		n.MoveToFirstChild ();
		PopulateNode (n.SelectChildren ("node", ""), tree);
		
	}
	
	void PopulateNode (XPathNodeIterator nodes, Node treeNode)
	{
		while (nodes.MoveNext ()) {
			XPathNavigator n = nodes.Current;
			string secNumber = n.GetAttribute ("number", ""),
				secName = n.GetAttribute ("name", "");
			
			Console.WriteLine ("\tSection: " + secNumber);
			treeNode.tree.HelpSource.PackFile (Path.Combine (basedir, secNumber + ".xml"), secNumber);
			
			Node thisNode = treeNode.LookupNode (secNumber + ": " + secName, "ecmaspec:" + secNumber);
			
			if (n.HasChildren)
				PopulateNode (n.SelectChildren ("node", ""), thisNode);
		}
	}

	public override void CloseTree (HelpSource hs, Tree tree)
	{
	}
}

public class EcmaSpecHelpSource : HelpSource {
	Encoding enc;
	
	public EcmaSpecHelpSource (string base_file, bool create) : base (base_file, create)
	{
		enc = new UTF8Encoding (false, false);
	}

	public override string GetText (string url, out Node match_node)
	{
		match_node = null;
		if (url.StartsWith ("ecmaspec:")) {
			match_node = MatchNode (Tree, url);
			return GetTextFromUrl (url);
		}
		
		if (url == "root:")
				return "<table width=\"100%\" bgcolor=\"#b0c4de\" cellpadding=\"5\"><tr><td><h3>C# Language Specification</h3></tr></td></table>";
		
		return null;
	}
	
	private Node MatchNode (Node node, string matchurl)
	{	
		foreach (Node n in node.Nodes) {
			if (matchurl == n.Element)
				return n;
			else if (matchurl.StartsWith (n.Element + ".") && !n.IsLeaf)
				return MatchNode (n, matchurl);
		}
		
		return null;
	}

	string GetTextFromUrl (string url)
	{
		Stream file_stream = GetHelpStream (url.Substring (9));
		if (file_stream == null)
			return null;
		
		return Htmlize (new XPathDocument (file_stream));
	}
	
	
	static XslTransform ecma_transform;

	static string Htmlize (XPathDocument ecma_xml)
	{
		if (ecma_transform == null){
			ecma_transform = new XslTransform ();
			System.Reflection.Assembly assembly = System.Reflection.Assembly.GetCallingAssembly ();
			Stream stream = assembly.GetManifestResourceStream ("ecmaspec-html.xsl");

			XmlReader xml_reader = new XmlTextReader (stream);
			ecma_transform.Load (xml_reader);
		}
		
		if (ecma_xml == null) return "";

		StringWriter output = new StringWriter ();
		ecma_transform.Transform (ecma_xml, null, output);
		
		return output.ToString ();
	}
}
}
